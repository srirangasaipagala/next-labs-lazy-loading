## Live Link
[github live link](https://theunhackable.github.io/next-labs-lazy-loading/)

[gitlab live link](https://next-labs-lazy-loading-srirangasaipagala-0edfa149417bd2dc46795c.gitlab.io/)

## Public API
 - [picsum.photos](https://picsum.photos/)


## Update!
The light house score of the mobile page is low because of the metamask extension that I have installed on my browser. I have created another light house report from chrome browser which I have uploaded as `light-house-mobile-report-update.pdf` please see that
## Can we automate the CI/CD?
`Yes` we can automate the CI/CD in gitlab using the `.gitlab-ci.yml` file and gitlab pages. This file contains set of instructions on what to do whenever there are changes in the repository.
